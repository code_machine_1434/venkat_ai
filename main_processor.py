#!/usr/bin/env python3

#================>Import Area<==================================================

#-------------Chat_bot----------------------------------------------------------
from venkat_skills.BOt import ask_bot
#-------------DuckDUCKGO Engine-------------------------------------------------
from venkat_skills.DuckDuckGo_Search import Internet_Search
#from venkat_skills.DuckDuckGo_Search import check as Internet_query_check
#-------------Speech to text synthesizer----------------------------------------
from venkat_skills.STT import hear
from venkat_skills.STT import init as initialize_stt
#-------------Game-handler------------------------------------------------------
from venkat_skills.Game_handler import game_chooser
from venkat_skills.Game_handler import check as game_check
#-------------Jokes-------------------------------------------------------------from TTS import speak
from venkat_skills.jokes import check as joke_check
from venkat_skills.jokes import ask as say_joke
#-------------MAth Handler------------------------------------------------------
from venkat_skills.math_handler import check as math_check
from venkat_skills.math_handler import evaluate as calculate
#-------------Youtube Handler---------------------------------------------------
from venkat_skills.youtube_handler import check as youtube_check
from venkat_skills.youtube_handler import play as youtube_play
#-------------web page and file launcher----------------------------------------
from venkat_skills.launcher import launcher as launcher
from venkat_skills.launcher import check as launcher_check
#-------------Wiki Search Client------------------------------------------------
from venkat_skills.wiki import ask_wiki
from venkat_skills.wiki import check as wiki_check
#------------2-Weather Skill of Venkat-------------------------------------------
from venkat_skills.weather import check as weather_check
from venkat_skills.weather import query_weather
#-------------Conventional Modules----------------------------------------------
from os import system
import pickle
#=============Initializing engines==============================================
app_engine=launcher()#Initializinf app-launcher engine
initialize_stt()#Initializes the sppech to text engine
#==========================>function definition area<===========================

def process(input_str):

    if launcher_check(inp_str):
        app_engine.app_launch(inp_str)

    elif youtube_check(input_str):
        youtube_play(input_str)

    elif math_check(input_str) :
        calculate(input_str)

    elif joke_check(input_str):
        say_joke()

    elif game_check(input_str):
        game_chooser()

    elif 'bye' in input_str.lower():
        raise RuntimeError('Execution Complete')

    elif weather_check(inp_str):
        query_weather(inp_str)

    elif wiki_check(input_str):
        ask_wiki(input_str)

    elif ' you ' in input_str.lower() or ' your ' in input_str.lower() or ' you'==input_str.lower()[-1:-5]:
        ask_bot(input_str)
        #print(3)
    else :        #print(3)
        duck=Internet_Search(input_str)
        duck.results()

#=============================>MAIN__AREA<======================================

with open('/Venkat_AI_essential_info.memory','rb') as file :
    path=pickle.load(file)#fetching path from base memory
    user_name=pickle.load(file)#fetchin user name from base memory
#initialize_stt()
print('\n\n')


try:
    with open(path+'/questions_asked.memory','ab+') as file :
        while True :
            inp_str=hear().strip().lower().replace('  ',' ').replace(' into ','*').replace(' X ',' * ').replace(' ^ ',' ** ').replace(' to the power ','**')
            process(inp_str)
            pickle.dump(inp_str,file)
except RuntimeError :
    print("\n\n\n\Thanks for using me!!!!!!!!!!!!!!!!!!!")
