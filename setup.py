import os
import time
def run(str1):
    temp=os.system(str1)

user_name=input('\n\nhow would you like me to call you:')

input('\n\nThe setup is about to start , please ensure that you have connected \nto the internet and then press enter to proceed!!')

print('\n\nInstalling essential packages \n\n')
run('apt-get install python3-pip git gcc make pkg-config automake libtool libasound2-dev libjack-jackd2-dev portaudio19-dev swig default-jdk libxml2-dev libxslt1-dev libffi-dev libssl-dev libpulse-dev python3-setuptools python3-dev' )
run('python3 -m pip install fuzzywuzzy[speedup] pymunk pygame PyAudio SpeechRecognition wikipedia')
#run('apt-get install git')
#run('apt-get install gcc make pkg-config automake libtool libasound2-dev')

#run('python3 -m pip install pymunk')
#run('python3 -m pip install pygame')
#run('apt-get install libjack-jackd2-dev portaudio19-dev')
#run('python3 -m pip install PyAudio')
#run('apt-get install swig')
#run('apt-get install libxml2-dev libxslt1-dev')
#run('apt-get install libffi-dev')
#run('apt-get install libssl-dev')
#run('apt-get install libpulse-dev')
#run('python3 -m pip install SpeechRecognition')


path=os.getcwd()
path='/'.join(path.split('/')[:3])+'/'
AI_path=path+'Venkat_AI/venkat_skills/'
run('chmod +x '+path+'Venkat_AI/'+'main_processor.py')
import pickle

with open('/Venkat_AI_essential_info.memory','wb') as file :
    pickle.dump(AI_path,file)
    pickle.dump(user_name,file)
with open(path+'.bashrc','a') as file :
    file.write('\nalias venkat=\''+path+'Venkat_AI/'+'main_processor.py\'')
#run('alias venkat="'+AI_path+'main_processor.py"')
print('\n\nSetup Completed')

t=input('\nFor venkat ai to work you must reboot your pc , shall i reboot it ?   (y/n):')
if 'n' in t :
    print('Ok system will wait for manual reboot')
elif 'y' in t :
    print('ok system will be rebooted')
    time.sleep(2)
    run('reboot')
else:
    print(' I didnt understand your choice please do a manual reboot')
