from bs4 import BeautifulSoup as bs
import requests
import webbrowser
#=============================TTS initialization================================
from venkat_skills.TTS import mary_tts # As the working directory is the location where main_processor.py is located
speech=mary_tts()
def speak(inp_str):
    speech.speak(inp_str)
#===============================================================================

def check(input_str):
    if input_str[:4]=='play':
        return(True)
    else:
        return(False)

def play(input_str):
    input_str=input_str[5:]
    input_str=input_str.replace(' ','+')
    base = "https://www.youtube.com/results?search_query="
    qstring = input_str
    r = requests.get(base+qstring)
    page = r.text
    soup=bs(page,'html.parser')
    vids = soup.findAll('a',attrs={'class':'yt-uix-tile-link'})
    for v in vids:
        url = 'https://www.youtube.com' + v['href']
        break
    webbrowser.open(url,new=2)
    speak("Hope you liked that!!")

#print(play(input()))
