from webbrowser import open as open_web_page
from fuzzywuzzy import process
from os import popen as execute_sys
from glob import glob#This command is to get a function globe which is capable of searching for files with given input string

#=============================TTS initialization================================
from venkat_skills.TTS import mary_tts # As the working directory is the location where main_processor.py is located
speech=mary_tts()
def speak(inp_str):
    speech.speak(inp_str)
#===============================================================================

def check(inp_str):#function to check if the inp_str matches for the query
    if inp_str[:5] == 'open ' or inp_str[:7]=='launch ':
        return(True)
    else:
        return(False)

def file_executo(inp):#The following function recievs a linux executable filename and executes it
    print("Opening %s"%(inp))
    execute_sys("`grep '^Exec' "+inp+" | tail -1 | sed 's/^Exec=//' | sed 's/%.//' | sed 's/^\"//g' | sed 's/\" *$//g'` &")

class launcher():

    def __init__(self):
        self.app_directories=tuple()
        self.apps=[]
        self.app_list()

    def app_list(self):
        #The below command gives us a lict of applications installed in the linux system
        self.app_directories=glob('/usr/share/applications/*.desktop')#This command will return a tuple which matches with the input string
        self.apps=[x.split('/')[-1][:-8] for x in self.app_directories]#This command filters out the app name alone from the app location names which was obtained from the previous command
        #print(self.apps)
        self.purpose_command= "`grep '^Exec' %s | tail -1 | sed 's/^Exec=//' | sed 's/%.//' | sed 's/^\"//g' | sed 's/\" *$//g'` &"

    def app_launch(self,inp_str):
        #The below if condition is put to just extract the name of the app alone to be extracted from the query
        if inp_str[0]=='o': #This is put in assumption that when a query passes through the check() function of this file , the first letter should be an 'o' when the first word of the query is open or a 'l' when the first word of the query is l
            inp_str=inp_str[5:]
        else:
            inp_str=inp_str[7:]
        #The process.extract(inp_str,self.apps) returns a list of tuples and the list will look like [('match1',<confidence in percentage>),('match2',<confidence in percentage>),.,.,...........]
        results=[x[0] for x in process.extract(inp_str,self.apps) if x[1]>80 ]#This command filters out the results with confidence value lesser than 80% to overcome inaccuracy of results
        #print(results)
        if results==[] :#When no matching results are found , a duckduckgo search with the query is invoked in the default browser
            open_web_page('https://www.duckduckgo.com/?q=%s&t'%(inp_str.replace(' ','+')))
            speak("I was not able to find a app for that in the local pc so let me open it in browser")

        elif len(results)>1 :#Just to handle multiple results with confidence greater than 80% confidence
            speak('Multiple matches where found ,kindly enter the serial number of your choice:')
            for i in range(len(results)):
                print('%d) %s'%(i,results[i]))
            try:
                choice=int(input('Enter the serial number of your choice:'))
                #file_executo(self.app_directories[self.apps.index(results[choice])])
                file_executo(self.app_directories[self.apps.index(results[choice])])
            except IndexError :
                file_executo(self.app_directories[self.apps.index(results[0])])
            except ValueError :
                file_executo(self.app_directories[self.apps.index(results[0])]) #Incase of invalid input sent by the user
            speak("Hope you like that!!")
        elif len(results)==1 :
            file_executo(self.app_directories[self.apps.index(results[0])])
            speak('Hope you like that!!')
#Testing area+++++++++++++++++++++++
#app_engine=launcher()
#app_engine.app_launch(input("Enter the name of the app to be launched:"))
