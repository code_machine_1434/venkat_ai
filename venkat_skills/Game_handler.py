import os
import time
import pickle
from fuzzywuzzy.StringMatcher import ratio

#=============================TTS initialization================================
from venkat_skills.TTS import mary_tts # As the working directory is the location where main_processor.py is located
speech=mary_tts()
def speak(inp_str):
    speech.speak(inp_str)
#===============================================================================


def check(input_str):
    l=['i am feeling bored','i am feeling lonely','i am feeling tired','random fun']
    for i in l :
        if ratio(input_str,i)>0.9:
            return(True)
    else:
        return(False)

def game_chooser():
    speak("Oh okay i have some games to cheer you up!!!")
    speak("Which of the following games do you wanna play\n\n\t1)Angry-Birds (python version)\n\t2)Super Mario\n\t3)Pacman\n\nEnter the serial number of your choice below")
    choice=input("Your choice")
    game_files={
    '1':'angry-birds-python/src/main.py',
    '2':'Mario-Level-1/mario_level_1.py',
    '3':'Pacman-Pygame/Main.py'
    }
    if choice not in ['1' , '2' , '3']:
        print('\nKindly enter a proper choice')
        choice='1'
    else:
        if choice=='1' :
            speak('\nMove your mouse for controls.. Game loading')
            time.sleep(3)
        with open('/Venkat_AI_essential_info.memory','rb') as file:
            path=pickle.load(file)
        temp=os.system('python3 '+path+'Games/'+game_files[choice])
        speak("Hope you liked that!!")
