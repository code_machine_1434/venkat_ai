import pickle
from fuzzywuzzy import process
from random import randint

#=============================TTS initialization================================
from venkat_skills.TTS import mary_tts # As the working directory is the location where main_processor.py is located
speech=mary_tts()
speech.start_serving()
def speak(inp_str):
    speech.speak(inp_str)
#===============================================================================

with open('/Venkat_AI_essential_info.memory','rb') as file :
    path=pickle.load(file)
    user_name=pickle.load(file)

with open(path+'Ai-chat.memory','rb') as file :
    questions=pickle.load(file)
    answers=pickle.load(file)

def ask_bot(input_str):
    best_match,confidence=process.extract(input_str,questions)[0]
    best_answers=answers[questions.index(best_match)]#
    output=best_answers[randint(0,len(best_answers)-1)]#tHIS LINE AND THE LINE ABOVE ARE USED TO CHOOSE A RANDOM ANS OUT OF MULTIPLE POSSIBLE ANSWERS FOR THE SAME QUESTION
    print(confidence)
    if confidence < 85 :
        speak("Sorry I don't get the query")
    else:
        speak(output)
