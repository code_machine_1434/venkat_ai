from math import *
from string import digits

#=============================TTS initialization================================
from venkat_skills.TTS import mary_tts # As the working directory is the location where main_processor.py is located
speech=mary_tts()
def speak(inp_str):
    speech.speak(inp_str)
#===============================================================================



MATH_FUNCS=[' log ',' sin ', ' cos ',' tan ']

def check(str1):
    if ' pi ' in str1:
        return(True)
    for i in MATH_FUNCS:
        if i in str1 :
            return(True)
    for i in str1 :
        if (i in '+-*/()')  :
            return(True)
    else:
        return(False)

def evaluate(func):
    for i in MATH_FUNCS :
        if i in func:
            index=func.index(i)+3
            func=func.replace(i,i[:-1]+'(')+' '
            #print(index,len(func))
            #print(range(index,len(func)))
            for i in range(index,len(func)):
            #    print(i)
            #    print(func[i])
                if func[i]==' ':
                    #print(func)
                    if i==len(func):
                        func=func[:i]+')'
                    else:
                        func=func[:i]+')'+func[i+1:]
                        #print(func)
                    break
            #print('crossed the for loop')
    func=func[7::]

    if '^' in func :
            for i in range(len(func)):
                if func[i] == "^" :
                    func = func[:i] + '**' + func[i+1:]
    if 'e' in func:
            for i in range(len(func)):
                if func[i] == 'e':
                    o, m = func[i - 1], func[i + 1]
                    if o in digits and m in digits :
                        func = func[:i] + ' * 10 ** ' + func[i + 1:]
                    else:
                        func = func[:i] + ' 2.71828 ' + func[i + 1:]
    try:
        speak('The answer is '+str(eval(func)))
    except SyntaxError:
        if ' of ' in func:
            speak('Try saying \'log 120\' instead of saying \'log of 120\'' )
        else:
            speak("I don't understand your mathematical question")
    except :
        speak("Complex mathematics is beyond my ability!!")
#print(evaluate(input('query:')))
