#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# HTTP + URL packages
import httplib2
from urllib.parse import urlencode, quote # For URL creation
import pickle
# To play wave files
import pygame
import math # For ceiling

from os import system
from threading import Timer
from time import sleep

with open('/Venkat_AI_essential_info.memory','rb') as file :
    path=pickle.load(file)

def server_start():
    system('/'.join(path.split('/')[:4])+'/mary/bin/./marytts-server')

class mary_tts():

    def __init__(self):
        # Mary server informations
        self.mary_host = "localhost"
        self.mary_port = "59125"
        self.query_hash={}

    def start_serving(self):
        from threading import Timer#to execute the fucntion in the background as the os.system is a ever running function due to which it halts the code execution in the middle!!
        Timer(0,server_start).start()
        sleep(10)
        self.speak("Hello I am Venkat ,I am here to assist you in this awesome day!!")

    def speak(self,inp,charecter='Venkat',freq=17000):
        print('%s :'%(charecter)+inp)
        self.query_hash = {"INPUT_TEXT":inp,
              "INPUT_TYPE":"TEXT", # Input text
              "LOCALE":"en_GB",
              "VOICE":"cmu-rms", # Voice informations  (need to be compatible)
              "OUTPUT_TYPE":"AUDIO",
              "AUDIO":"WAVE", # Audio informations (need both)
              }
        query = urlencode(self.query_hash)

    # Run the query to mary http server
        h_mary = httplib2.Http()
        resp, content = h_mary.request("http://%s:%s/process?"%(self.mary_host,self.mary_port),"POST",query)
        # Decode the wav file or raise an exception if no wav files
        if (resp["content-type"] == "audio/x-wav"):

    # Write the wav file
            f = open("output_wav.wav", "wb")
            f.write(content)
            f.close()

    # Play the wav file
            pygame.mixer.init(frequency=freq) # Initialise the mixer
            s = pygame.mixer.Sound("output_wav.wav")
            s.play()
            pygame.time.wait(int(math.ceil(s.get_length() * 1000)))

        else:
            raise ValueError(content)

#Module Testing area+++++++++++++++++++++++++++++==

#speech_engine=mary_tts()
#speech_engine.speak("Hello world!!")
