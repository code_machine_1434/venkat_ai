import pickle
from random import randint
from fuzzywuzzy.StringMatcher import ratio
#=============================TTS initialization================================
from venkat_skills.TTS import mary_tts # As the working directory is the location where main_processor.py is located
speech=mary_tts()
def speak(inp_str):
    speech.speak(inp_str,'Minion',22000)
#===============================================================================

def check(input_str):
    if ratio('tell me a joke',input_str)>0.9 or ratio('say me a joke',input_str)>0.9:
        return(True)
    else:
        return(False)

    #engine.speak_now()

with open('/Venkat_AI_essential_info.memory','rb') as file :
    path=pickle.load(file)

with open(path+'jokes.memory', 'rb') as file :
    jokes=pickle.load(file)
    #print(jokes)

def ask():
    joke_no=randint(0,len(jokes)-2)
    speak(jokes[joke_no][0]+' ....... '+jokes[joke_no][1])
