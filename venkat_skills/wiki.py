#========Importing wikipedia module=============================================
from wikipedia import search
from wikipedia import summary
from wikipedia import PageError
#========Importing mary tts client==============================================
from venkat_skills.TTS import mary_tts
speech=mary_tts()
def speak(inp_str):
    speech.speak(inp_str,'Wiki',14000)
#========GenaralFunctions=======================================================
def check(inp_str):
    if 'ask wiki about '==inp_str[:15] :
        return(True)
    else:
        return(False)

def ask_wiki(inp_str):
    inp_str=inp_str[15:]#Just the statement to be searched alone is isolated from the query passed to venkat
    try:
        #print(inp_str)
        #print(summary(inp_str))
        speak(summary(inp_str).split('\n')[0])#The summary's first paragraph alone is taken into consideration
    except PageError :#Incase it doesnt know what to do with the given input string then this exception statement get's excuted
        best_matches=search(inp_str)
        if len(best_matches)==0 :
            speak('Sorry no matching results found!!')
        else:
            speak("\nMultiple Matches were found ,Kindly enter your choice's serial number")
            for i in range(len(best_matches)):
                speak("%d) %s"%(i,best_matches[i]))
            try:
                choice=int(input("Enter your choice:"))
                speak(summary(best_matches[choice]).split('\n')[0])#The summary's first paragraph alone is taken into consideration
            except KeyError:
                speak(best_matches[0].split('\n')[0])#The summary's first paragraph alone is taken into consideration
            except ValueError:
                speak(best_matches[0].split('\n')[0])#The summary's first paragraph alone is taken into consideration
#ask_wiki(input("Enter your query:"))
