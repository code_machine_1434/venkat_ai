import urllib.request
import urllib.parse
from bs4 import BeautifulSoup as soup
null=None#some json variable are none.. to support it it is present
#=============================TTS initialization================================
from venkat_skills.TTS import mary_tts # As the working directory is the location where main_processor.py is located
speech=mary_tts()
def speak(inp_str):
    speech.speak(inp_str,'Duck',22000)
#===============================================================================
'''
def check(input_str):
    if (('who' in input_str) or ('what' in input_str) or ('when' in input_str)) and ((' is ' in input_str) or (' are ' in input_str )):
    	return(True)
    else:
    	return(False)
'''
def string_processor(input_str):
    if ' is ' in input_str :
        return(input_str.split(' is ')[1])
    elif ' are ' in input_str :
        return(input_str.split(' are ')[1])
    else:
        return(input_str)
class Internet_Search():

	def __init__(self,query):
		self.query=string_processor(query)

	def encode(self,**kwargs):
		safesearch = '1' #if safesearch else '-1'
		html = '0' #if html else '1'
		meanings = '0' #if meanings else '1'
		params = {'q': self.query,'o': 'json','kp': safesearch,'no_redirect': '1','no_html': html,'d': meanings}
		params.update(kwargs)
		encparams = urllib.parse.urlencode(params)
		url = 'http://www.duckduckgo.com/?' + encparams
		return(url)

	class Abstract(object):
		def __init__(self,json):
			self.html = json['Abstract']
			self.text = json['AbstractText']
			self.url = json['AbstractURL']
			self.source = json['AbstractSource']

	def results(self):
		try:
			re=urllib.request.Request(encode(self.query),headers={'User-Agent': useragent})
		except:
			try:
				re=urllib.request.Request(self.encode())
				response=urllib.request.urlopen(re)
				web_content=(response.read())
				web_content=str(web_content,'utf-8')#coverting bytes to string
				json=dict(eval(web_content))
				response.close()
				content=self.Abstract(json)
				if content.text!='':
					speak(content.text)
				else:
					import webbrowser
					url='http://www.duckduckgo.com/?q='
					t=[ ['%', '%25'],['+','%2B'],[' ', '+'], ['!', '%21'], ['"', '%22'], ['#', '%23'], ['$', '%24'], ['&', '%26'], ["'", '%27'], ['(', '%28'], [')', '%29'], [',', '%2C'], ['/', '%2F'], [':', '%3A'], [';', '%3B'], ['<', '%3C'], ['=', '%3D'], ['>', '%3E'], ['?', '%3F'], ['@', '%40'], ['[', '%5B'], ['\\', '%5C'], [']', '%5D'], ['^', '%5E'], ['`', '%60'], ['{', '%7B'], ['|', '%7C'], ['}', '%7D'], ['~', '%7E']]
					for i in t:
						self.query=self.query.replace(i[0],i[1])
					url+=self.query+'&t'
					webbrowser.open(url,new=2)
					#print(url)
					speak('Showing Results')
			except urllib.error.URLError :
				speak("Sorry no Results")
