import speech_recognition as sr
from speech_recognition import UnknownValueError
#from TTS import speak

r = sr.Recognizer()
def init():
    with sr.Microphone() as source:
        global audio
        audio = r.adjust_for_ambient_noise(source)
        #The energy threshold speaks about the min volume for the recording to stop
        #The r.adjust_for_ambient_noise(source) detects the min volume for recording to stop
        if r.energy_threshold<300 :#In some cases the energy threshold is leeser thn 300 in that case threshold value is set to 300 to avoid continuous recording
            r.dynamic_energy_threshold=False#Automatic adjustment of threshold value is disabled!!
            r.energy_threshold=300
            print("Threshold value is changed!")


def hear():
    input('press enter to speak')
# obtain audio from the microphone
    with sr.Microphone() as source:

  #audio = r.adjust_for_ambient_noise(source)
        print("speak now and wait!\n")
  #audio=r.listen(source)

        audio = r.record(source,duration=5)
        print('audio recorded ,now listening')

# recognize speech using google
    try:
        detected_sentence=r.recognize_google(audio)
        print('Query:',  detected_sentence)
        return(detected_sentence)
    except UnknownValueError :
        return("Sorry that was a technical glitch")
 #return('Sorry I cannot do this !! You must be connected to internet!!')
#speak(hear())

#print('Take 2')
#print(hear())
#speak(hear())
